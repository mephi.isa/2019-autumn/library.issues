TYPE_PRINT = 0
TYPE_ELECTRONIC = 1


class GenericError(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class Book:
    def __init__(self,
                 id: int,
                 title: str,
                 author: str):
        self.id: int = id
        self.title: str = title
        self.author: str = author
        self.publishingOffice: list = []

    def set_id(self, id: int):
        self.id = id

    def get_id(self) -> int:
        return self.id

    def set_title(self, title: str):
        self.title = title

    def get_title(self) -> str:
        return self.title

    def set_author(self, author: str):
        self.author = author

    def get_author(self) -> str:
        return self.author

    def get_publishingOffice(self) -> list:
        return self.publishingOffice


class ReadingFund:
    def __init__(self):
        self.books: list = []

    def get_book(self) -> list:
        return self.books

    def add_book(self, book: object):
        isExist = True
        for v in range(len(self.books)):
            if self.books[v].get_id() == book.get_id() or self.books[v].get_title() == book.get_title():
                if (book.type == TYPE_ELECTRONIC):
                    return GenericError("Book with given id or title already exists in this fund")
                    isExist = False
                else:
                    book.incrimentAmount()
                    isExist = False
        if (isExist):
            self.books.append(book)

    def remove_book(self, book: Book):
        self.books.remove(book)

    def search_book_id(self, book_id: int, type: int) -> Book:
        for i, book in enumerate(self.books):
            if (book.type == type and  book.id == book_id):
                return book

    def search_book_title(self, book_title: str, type: int) -> Book:
        for i, book in enumerate(self.books):
            if (book.type == type and book.title == book_title):
                return book

    def search_book_author(self, book_author: str, type: int) -> Book:
        for i, book in enumerate(self.books):
            if (book.type == type and book.author == book_author):
                return book

class PublishingOffice:
    def __init__(self, id: int,
                 name: str,
                 phone: str):
        self.id: int = id
        self.name: str = name
        self.phone: str = phone

    def set_id(self, id: int):
        self.id = id

    def get_id(self) -> int:
        return self.id

    def set_name(self, name: str):
        self.name = name

    def get_name(self) -> str:
        return self.name

    def set_phone(self, phone: str):
        self.phone = phone

    def get_phone(self) -> str:
        return self.phone


class Order:
    def __init__(self, id: int,
                 status: str):
        self.id: int = id
        self.status: str = status
        self.books: list = []

    def set_id(self, id: int):
        self.id = id

    def get_id(self) -> int:
        return self.id

    def set_status(self, status: str):
        self.status = status

    def get_status(self) -> str:
        return self.status

    def get_books(self) -> list:
        return self.books

    def add_book(self, book: object):
        isExist = True
        for v in range(len(self.books)):
            if self.books[v].get_id() == book.get_id() or self.books[v].get_title() == book.get_title():
                if(book.type == TYPE_ELECTRONIC):
                    raise GenericError("Book with given id or title already exists in this fund")
                    isExist = False
                else:
                    book.incrimentAmount()
                    isExist = False
        if(isExist):
            self.books.append(book)


    def search_book_id(self, book_id: int, type: int) -> Book:
        for i, book in enumerate(self.books):
            if (book.type == type and  book.id == book_id):
                return book

    def search_book_title(self, book_title: str, type: int) -> Book:
        for i, book in enumerate(self.books):
            if (book.type == type and book.title == book_title):
                return book

    def search_book_author(self, book_author: str, type: int) -> Book:
        for i, book in enumerate(self.books):
            if (book.type == type and book.author == book_author):
                return book

    # def search_book(self, **kwargs):
    #     our_book: Book = None
    #     if "id" in kwargs:
    #         for i, book in enumerate(self.books):
    #             if book.get_id() == id:
    #                 our_book = book
    #                 break
    #     elif "title" in kwargs:
    #         for i, book in enumerate(self.books):
    #             if book.get_title() == kwargs["title"]:
    #                 our_book = book
    #                 break
    #     return our_book

    def remove_book(self, book: Book):
        self.books.remove(book)


class electronicVersion(Book):
    def __init__(self, id: int,
                 title: str,
                 author: str,
                 hyperlink: str,
                 type: int):
        super().__init__(id, title, author)
        self.hyperlink: str = hyperlink
        self.type: int = TYPE_ELECTRONIC



    def set_hyperlink(self, hyperlink: str):
        self.hyperlink = hyperlink

    def get_hyperlink(self) -> str:
        return self.hyperlink
    def getAll(self):
        return [self.id, self.author, self.title, self.hyperlink]

class printVersion(Book):
    def __init__(self, id: int,
                 title: str,
                 author: str,
                 amount: int,
                 type:int):
        super().__init__(id, title, author)
        self.amount: int = amount
        self.type:int=TYPE_PRINT

    def set_amount(self, amount: str):
        self.hyperlink = amount
    def incrimentAmount(self):
        self.amount +=1;
    def get_amount(self) -> str:
        return self.amount

    def getAll(self):
        return [self.id, self.author, self.title, self.amount]
class Request:
    def __init__(self, id: int, publishingOfficeName: str):
        self.publishingOfficeName: str = publishingOfficeName
        self.id: int = id
        self.book: list = []

    def set_id(self, id: int):
        self.id = id

    def get_id(self) -> int:
        return self.id

    def set_publishingOfficeName(self, publishingOfficeName: str):
        self.publishingOfficeName = publishingOfficeName

    def get_publishingOfficeName(self) -> str:
        return self.publishingOfficeName

    def get_book(self) -> list:
        return self.book

    def add_book(self, book: object):
        self.book.append(book)

    def remove_book(self, book: Book):
        self.book.remove(book)

    def search_book(self, **kwargs):
        out_book: Book = None
        if "title" in kwargs:
            for i, book in enumerate(self.book):
                if book.get_title() == kwargs["title"]:
                    out_book = book
                    break
                return out_book

