
TYPE_PAPER = 0
TYPE_ELECTRONIC = 1


class GenericError(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class Book:
    def __init__(self,
                 id: int,
                 title: str,
                 author: str):
        self.id:int = id
        self.title:str = title
        self.author:str = author
        self.publishingOffice:list=[]

    def set_id(self, id: int):
        self.id = id

    def get_id(self) -> int:
        return self.id

    def set_title(self, title: str):
        self.title = title

    def get_title(self) -> str:
        return self.title

    def set_author(self, author: str):
        self.author = author

    def get_author(self) -> str:
        return self.author

    def get_publishingOffice(self) -> list:
        return self.publishingOffice


class ReadingFund:
    def __init__(self ):
        self.books:list = []

    def get_book(self) -> list:
        return self.books

    def add_book(self, book: Book):
        for k, v in enumerate(self.books):
            if v.get_id() == book.get_id() or v.get_title() == book.get_title():
                raise GenericError("Book with given id or title already exists in this fund")
        self.books.append(book)

    def remove_book(self, book: Book):
        self.__books.remove(book)

    def search_book(self, **kwargs):
        out_book: Book = None
        if "title" in kwargs:
            for i, book in enumerate(self.book):
                if book.get_title() == kwargs["title"]:
                    out_book = book
                    break
        return out_book


class PublishingOffice:
    def __init__(self,id: int,
                 name: str,
                 phone: str):
        self.id:int = id
        self.name:str = name
        self.phone:str = phone

    def set_id(self, id: int):
        self.id = id

    def get_id(self) -> int:
        return self.id

    def set_name(self, name: str):
        self.name = name

    def get_name(self) -> str:
        return self.name

    def set_phone(self, phone: str):
        self.phone = phone

    def get_phone(self) -> str:
        return self.phone


class Order:
    def __init__(self,     id: int,
                 status: str):
     self.id:int=id
     self.status:str=status
     self.book:list=[]

    def set_id(self, id: int):
        self.id = id

    def get_id(self) -> int:
        return self.id

    def set_status(self, status: str):
        self.status = status

    def get_status(self) -> str:
        return self.status

    def get_book(self) -> list:
        return self.book

    def add_book(self, book:Book):
        self._books.append(book)

    def search_book(self, **kwargs):
        out_book: Book = None
        if "title" in kwargs:
            for i, book in enumerate(self.book):
                if book.get_title() == kwargs["title"]:
                    out_book = book
                    break
        return out_book


class electronicVersion(Book):
    def __init__(self, id: int,
                 title: str,
                 author: str,
                 hyperlink: str):
        super().__init__(id, title,author)
        self.hyperlink:str=hyperlink

    def set_hyperlink(self, hyperlink: str):
        self.hyperlink = hyperlink

    def get_hyperlink(self) -> str:
        return self.hyperlink

class printVersion(Book):
    def __init__(self, id: int,
                 title: str,
                 author: str,
                 amount: int):
        super().__init__(id, title,author)
        self.amount:int=amount

    def set_amount(self, amount: str):
        self.hyperlink = amount

    def get_amount(self) -> str:
        return self.amount

class Request:
    def __init__(self, id: int, publishingOfficeName: str ):
             self.publishingOfficeName:str = publishingOfficeName
             self.id:int=id
             self.book:list=[]

    def set_id(self, id: int):
        self.id = id

    def get_id(self) -> int:
        return self.id

    def set_publishingOfficeName(self, publishingOfficeName: str):
        self.publishingOfficeName = publishingOfficeName

    def get_publishingOfficeName(self) -> str:
        return self.publishingOfficeName

    def get_book(self) -> list:
        return self.book

    def add_book(self, book: Book):
        self._books.append(book)

    def remove_book(self, book: Book):
        self.__books.pop(book)

    def search_book(self, **kwargs):
        out_book: Book = None
        if "title" in kwargs:
            for i, book in enumerate(self.book):
                if book.get_title() == kwargs["title"]:
                    out_book = book
                    break
        return out_book