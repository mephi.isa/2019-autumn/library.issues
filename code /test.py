import unittest
from classes import ReadingFund, Book, PublishingOffice, Order, Request, electronicVersion, printVersion


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.publishingOffice = PublishingOffice(
            id=1,
            name="Piter",
            phone="8-925-2039852"
        )

        self.ebook = electronicVersion(
            id=1,
            title="C++ language",
            author="M.Khalid",
            hyperlink= "dfghjklokjhfdxcvbnm",
            type=1
        )

        self.pbook = printVersion(
            id=2522,
            title="C++ language1",
            author="M.Khalid",
            amount=5,
            type=0

        )

        self.order = Order(
            id=1,
            status='new'
        )


        self.request = Request(
            publishingOfficeName="Piter",
            id=52
        )

        self.readingFund = ReadingFund()
        self.readingFund.add_book(self.ebook)
        self.readingFund.add_book(self.pbook)
        self.request.add_book(self.pbook)
        self.request.add_book(self.ebook)
        self.order.add_book(self.pbook)
        self.order.add_book(self.ebook)


    def test_pbook_id(self):
        self.pbook.set_id(22)
        print(self.readingFund.search_book_id(self.pbook.id, 0))

    def test_ebook_id(self):
        self.ebook.set_id(33)
        self.assertEqual(self.readingFund.search_book_id(book_id=1,type=1), None)

    def test_ebook_id2(self):
        self.ebook.set_id(33)
        self.assertEqual(self.readingFund.search_book_id(book_id=33, type=1), self.ebook)

    def test_book_title(self):
        self.ebook.set_title("Java")
        self.readingFund.search_book_title(book_title=self.ebook.get_title(),type=1)

    def test_book_author(self):
        self.ebook.set_author("Mentov")
        self.readingFund.search_book_author(book_author=self.ebook.get_author(),type=1)

    def test_fund_book_add(self):
        self.ebook1 = electronicVersion(
            id=2,
            title="Kotlin language",
            author="M.Remak",
            hyperlink="dfghjklokjhfdxcvbnm",
            type=1
        )
        self.readingFund.add_book(self.ebook1)
        # print(self.readingFund.get_book()[1].title)
        self.assertEqual(len(self.readingFund.get_book()), 3)


    def test_publishingOffice_id(self):
        self.publishingOffice.set_id(103)
        self.assertEqual(self.publishingOffice.get_id(),103)

    def test_publishingOffice_name (self):
        self.publishingOffice.set_name('Samara')
        self.assertEqual(self.publishingOffice.get_name(),'Samara')

    def test_puboffice_phone (self):
        self.publishingOffice.set_phone('8-720-320-8497')
        self.assertEqual(self.publishingOffice.get_phone(),'8-720-320-8497')

    def test_order_add_book(self):
        self.order.add_book(electronicVersion(
            id=222,
            title="C language",
            author="Robim",
            hyperlink="dfghjklovhjjhgcghjhvckjhfdxcvbnm",
            type=1
        ))
        self.assertEqual(len(self.order.get_books()), 3)

    def test_order_add_book2(self):
        self.order.add_book(printVersion(
            id=2,
            title="C++ language",
            author="M.Remak",
            amount=5,
            type=0

        ))
        self.assertIsNotNone(
            self.order.search_book_title(book_title=self.pbook.get_title(),type=0))

    def test_order_id (self):
        self.order.set_id(366)
        self.assertEqual(self.order.get_id(),366)

    def test_order_status (self):
        self.order.set_status('old')
        self.assertEqual(self.order.get_status(),'old')

    def test_fund_remove_book(self):
        self.readingFund.remove_book(self.pbook)
        self.assertIsNone(self.readingFund.search_book_title(self.pbook.get_title(),type=0))

    def test_fund_search_book(self):
        self.readingFund.search_book_title(self.pbook,type=0)
        self.assertIsNone(self.readingFund.search_book_title(book_title="C++ language",type=0))

    def test_order_remove_book(self):
        self.order.remove_book(self.pbook)
        self.assertIsNone(self.order.search_book_title(self.pbook.get_title(),type=0))

    def test_request_book(self):
        self.request.set_id(66)
        self.assertEqual(self.request.get_id(),66)

    def test_request_book2(self):
        self.request.set_publishingOfficeName('American language')
        self.assertEqual(self.request.get_publishingOfficeName(),'American language')

    def test_request_add_book(self):
        self.request.add_book(electronicVersion(
            id=3,
            title="html",
            author="Maks",
            hyperlink="dfghjklokjhfdxcvbnm",
            type=1
        ))
        self.assertIsNone(
        self.request.search_book(title=self.ebook.get_title(), type=1))

    def test_request_remove_book(self):
        self.request.remove_book(self.pbook)
        self.assertIsNotNone(self.order.search_book_title(self.pbook.get_title(),type=0))


if __name__ == '__main__':
    unittest.main()

